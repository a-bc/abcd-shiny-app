
## Details

* Project name: ABCD Behaviour Change app
* Project code: __MAL0021__

## Project brief

" _Basically, there are two main components: an editable table/grid (fixed number of columns, flexible number of rows), and a diagram._

_People can fill the table either manually; by importing .csv or .xls(x) (basically, just using `rio` maybe); and by copy-pasting a link to a google sheet._

_Once the table is there they can continue editing and/or download the contents and/or create a diagram, which they can then view and download._ "

## Project notes

__Main functionality__
* Users can import data from a file (implemented using `rio`) or a Google spreadsheet, and data is displayed in the table. For clarity, the data source is defined in the text above the table.
* The table is editable by double clicking on the cell you wish to edit - the edited table can then be used to update the graph, or can be download for reproducibility. 
* Users can add a row to the table, with initalilises with "Click to edit" as the cell values.
* The graph itself can be download in a range of formats (PNG, PDF, SVG, PS).

__Extra elements__
* User friendly navigation has been implemented - the Graph tab, along with the table and associated buttons, are hidden on start to help guide users through the process. For example, clicking on "Generate plot" automatically show the "Graph" tab and moves the user to this tab. 
* Loading screens have been implemented.
* The app title (top left hand corner) along with the text on the "Welcome" tab are editable without needing to edit the Shiny code (from the `info.txt` and `intro.md` files in the `www/` folder respectively).
* Nice error handling has been implemented (i.e. informative errors, recommending that users check that the Google sheet is not private, etc.)



