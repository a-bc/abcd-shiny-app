---
title: "info"
output: html_document
---

# Welcome to the ABCD app!

ABCD stands for "Acyclic Behavior Change Diagram": a visual representation of the causal (i.e. what influences what) and structural (i.e. what consists of what)  assumptions underlying a behavior change intervention.

This online app allows you to generate an ABCD from an ABCD matrix: a spreadsheet with seven columns, where every row represents one causal-structural chain (see below for more information).

## How to use this app

If you click the Data tab at the top, you can either enter the link to a Google Sheet (great if you collaborate on your ABCD - make sure it's publicly viewable!) or upload a spreadsheet in .xlsx (Excel) or .csv (Comma Separated Values) format.

Instead of importing a ABCD matrix you created elsewhere, you can also edit it in this app by going directly to the Data tab and editing the table (and then download the matrix and the plot).

The app will show the ABCD matrix it imports, and if that looks good, you can click "Generate plot" to produce the Acyclic Behavior Change Diagram. You can then download the ACBD in various formats (as a PDF, as a rastered PNG file, or vectorized as SVG or PS file).

## Example ABCD

To look at a simple example, you can use a fictional intervention targeting ecstasy use, specifically, preventing people who use ecstasy from dosing too highly. The link to that Google Sheet is https://docs.google.com/spreadsheets/d/1iZpCcUDtqVfHvoSmWv5nDeRqLBX31K4iSFN4dJFOtqw/edit?usp=sharing, which you can directly copy-paste in the Data tab to view the ABCD matrix for that fictional mini-intervention, and click "Generate Plot" to view the ABCD itself.

## The ABCD matrix

The ABCD matrix is a spreadsheet with seven columns, where every row represents one causal-structural chain. The ABCD matrix is a machine-readable way to clearly specify exactly what your intervention will target and why you assume the intervention will work.

The seven columns in the ABCD matrix are, from left to right:

 - **Behavior Change Principles (BCPs)**: The specific
     psychological principles engaged to influence the relevant
     sub-determinants, usually selected using the determinants
     to which the sub-determinants 'belong'. These are also
     known as methods of behavior change in the Intervention
     Mapping framework, or behavior change techniques, BCTs,
     in the Behavior Change Wheel approach. For a list of 99
     BCPs, see [Kok et al. (2016)](https://osf.io/ng3xh/).
 - **Conditions for effectiveness**: The conditions
     that need to be met for a Behavior Change Principle (BCP) to
     be effective. These conditions depend on the specific
     underlying Evolutionary Learning Processes (ELPs) that the
     BCP engages ([Crutzen & Peters, 2018](http://dx.doi.org/10.1080/17437199.2017.1362569)). If the conditions for
     effectiveness (called *parameters for use* in the
     Intervention Mapping framework) are not met, the method will
     likely not be effective, or at least, not achieve its
     maximum effectiveness.
 - **Applications**: Since BCP's describe aspects of
     human psychology in general, they are necessarily formulated
     on a generic level. Therefore, using them in an intervention
     requires translating them to the specific target population,
     culture, available means, and context. The result of this
     translation is the application of the BCP. Multiple BCPs can
     be combined into one application; and one BCP can be applied in
     multiple applications (see [Kok, 2014](https://doi.org/10.31234/osf.io/r78wh)).
 - **Sub-determinants**: Behavior change interventions
     engage specific aspects of the human psychology (ideally, they
     specifically, target those aspects found most important in
     predicting the target behavior, as can be established with
     CIBER plots ([Crutzen, Peters & Noijen, 2017](https://doi.org/10.3389/fpubh.2017.00165)). These aspects are
     called sub-determinants (the Intervention Mapping framework
     references *Change Objectives*, which are sub-determinants
     formulated according to specific guidelines). In some
     theoretical traditions, sub-determinants are called *beliefs*.
 - **Determinants**: The overarching psychological constructs that
     are defined as clusters of specific aspects of the human
     psychology that explain humans' behavior (and are targeted
     by behavior change interventions). Psychological theories
     contain specific definitions of such determinants, and make
     statements about how they relate to each other and to human
     behavior. There are also theories (and exists empirical
     evidence) on how these determinants can be changed (i.e. BCPs),
     so althought the sub-determinants are what is targeted in an
     intervention, the selection of feasible BCPs requires knowing
     to which determinants those sub-determinants belong.
 - **Sub-behaviors**: The specific sub-behaviors that often
     underlie (or make up) the ultimate target behavior (called
     "Performance objectives" in Intervention Mapping). These are
     distinguished from the overarching target behavior because
     the relevant determinants of these sub-behaviors can be
     different: for example, the reasons why people do or do not
     *buy* condoms can be very different from the reasons why they
     do or do not *carry* condoms or why they do or do not
     *negotiate* condom use with a sexual partner.
 - **Behavior**: The ultimate target behavior of the intervention,
     usually an umbrella that implicitly contains multiple
     performance objectives.

For more information, see the [Book of Behavior Change](https://bookofbehaviorchange.com).
